﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTTPLib;
using System.Web;
using System.IO;

namespace HTTPMethods
{
    public class GetMethod : HTTPLib.IHTTPMethodHandler
    {
        public string MethodName
        {
            get
            {
                return "GET";
            }

        }
        // Generate the response to an HTTP request using this method. Assumes
        // the method name in the request line matches the name of this method.
        public HTTPResponse GenResponse(IServerState issPara)
        {
            string strAbsolutePath = issPara.ConfigDict["DocumentRoot"] + HttpUtility.UrlDecode(issPara.Request.URI);
            if (File.Exists(strAbsolutePath))
            {
                string strStatus = "HTTP/1.1 200 OK";
                List<string> lstrHeaders = new List<string>();
                Dictionary<string, string> dictMIMETypes = new Dictionary<string, string>();
                dictMIMETypes.Add(".htm", "text/html");
                dictMIMETypes.Add(".html", "text/html");
                dictMIMETypes.Add(".css", "text/css");
                dictMIMETypes.Add(".js", "text/javascript");
                dictMIMETypes.Add(".gif", "image/gif");
                dictMIMETypes.Add(".jpg", "image/jpeg");
                dictMIMETypes.Add(".jpeg", "image/jpeg");
                dictMIMETypes.Add(".png", "image/png");

                lstrHeaders.Add("Connection: close");
                FileStream strmRetriveFile = File.Open(strAbsolutePath, FileMode.Open, FileAccess.Read);
                lstrHeaders.Add("Content-Length: " + strmRetriveFile.Length.ToString());
                string strContentType = "text/plain";
                string strExtension = Path.GetExtension(strAbsolutePath);
                if (dictMIMETypes.Keys.Contains(strExtension))
                {
                    strContentType = dictMIMETypes[strExtension];
                }
                lstrHeaders.Add("Content-Type: " + strContentType);
                string strShortDate = DateTime.Now.ToString("R");
                lstrHeaders.Add("Date: " + strShortDate);
                lstrHeaders.Add("Server: " + issPara.ConfigDict["ServerName"]);

                HTTPResponse hrResponse = new HTTPResponse(strStatus, lstrHeaders, strmRetriveFile);
                return hrResponse;
            }
            else
            {
                string strStatus = "HTTP/1.1 404 Not Found";
                List<string> lstrHeaders = new List<string>();
                string strShortDate = DateTime.Now.ToString("R");
                lstrHeaders.Add("Date: " + strShortDate);
                lstrHeaders.Add("Server: " + issPara.ConfigDict["ServerName"]);
                lstrHeaders.Add("Connection: close");
                HTTPResponse hrResponse = new HTTPResponse(strStatus, lstrHeaders, null);
                return hrResponse;
            }  
        }
    }
    public class OptionsMethod : HTTPLib.IHTTPMethodHandler
    {
        public string MethodName
        {
            get
            {
                return "OPTIONS";
            }

        }
        // Generate the response to an HTTP request using this method. Assumes
        // the method name in the request line matches the name of this method.
        public HTTPResponse GenResponse(IServerState issPara)
        {
            string strStatus = "HTTP/1.1 200 OK";
            List<string> lstrHeaders = new List<string>();
            string strShortDate = DateTime.Now.ToString("R");
            lstrHeaders.Add("Date: " + strShortDate);
            lstrHeaders.Add("Server: " + issPara.ConfigDict["ServerName"]);
            string strAvaiMethod = "";
            foreach (var plugin in issPara.PluginDict.Values)
            {
                strAvaiMethod += plugin.MethodName + " ";
            }
            lstrHeaders.Add("Allow: " + strAvaiMethod.TrimEnd());
            lstrHeaders.Add("Connection: close");
            HTTPResponse hrResponse = new HTTPResponse(strStatus, lstrHeaders, null);
            return hrResponse;
        }
    }
}
