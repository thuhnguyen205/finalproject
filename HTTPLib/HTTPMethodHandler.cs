﻿using System;
using System.Collections.Generic;

namespace HTTPLib {
    public interface IHTTPMethodHandler {
        // The name of the method handled. Must be all uppercase letters.
        string MethodName {
            get;
        }

        // Generate the response to an HTTP request using this method. Assumes
        // the method name in the request line matches the name of this method.
        HTTPResponse GenResponse(IServerState issPara);
    }
    public interface IServerState
    {
        HTTPRequest Request
        {
            get;
        }
        Dictionary<string, string> ConfigDict
        {
            get;
        }
        Dictionary<string, IHTTPMethodHandler> PluginDict
        {
            get;
        }
    }
}
