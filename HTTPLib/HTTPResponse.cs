﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HTTPLib {
    public class HTTPResponse {
        // Fields.
        // Status line.
        private string strStatus;
        // List of headers.
        private List<string> lstHeaders;
        // The stream to get the body data from.
        private Stream strmBody;

        // The constructor.
        public HTTPResponse(string strS, List<string> lstH, Stream strmB)
        {
            // Initialize fields from the parameters.
            strStatus = strS;
            lstHeaders = lstH;
            strmBody = strmB;
        }

        // Send this response out over the given network strea.
        public void Send(Stream strmOut)
        {
            string strOneLine = strStatus + "\r\n";
            byte[] byRespone = Encoding.ASCII.GetBytes(strOneLine);
            strmOut.Write(byRespone, 0, byRespone.Length);

            foreach (var strHeader in lstHeaders)
            {
                strOneLine = strHeader + "\r\n";
                byRespone = Encoding.ASCII.GetBytes(strOneLine);
                strmOut.Write(byRespone, 0, byRespone.Length);
            }

            strOneLine = "\r\n";
            byRespone = Encoding.ASCII.GetBytes(strOneLine);
            strmOut.Write(byRespone, 0, byRespone.Length);

            if (strmBody != null)
            {
                byte[] byBuffer = new byte[1024];
                int iNumBytesRead = 0;
                while ((iNumBytesRead = strmBody.Read(byBuffer, 0, byBuffer.Length)) > 0)
                {
                    strmOut.Write(byBuffer, 0, iNumBytesRead);
                }
                strmBody.Close();
            }
           
        }
    }
}
