﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using HTTPLib;
using System.IO;
using System.Reflection;


namespace HTTPServer {
    public partial class Form1 : Form {
        // Fields 
        private Dictionary<string, string> dictConfig = null;
        private Dictionary<string, IHTTPMethodHandler> dictPlugins = null;
        private Socket socListen = null;

        public Form1() {
            InitializeComponent();
        }
        private void vLoadPlugins()
        {
            // Loop through all of the files in the plugin directory.
            string[] strPluginFiles = Directory.GetFiles("Plugins");
            foreach (string strOneFile in strPluginFiles)
            {
                // Check that this is a library file (i.e. DLL file).
                string strExtension = Path.GetExtension(strOneFile);
                if (strExtension.ToLower() == ".dll")
                {
                    // Get a list  of all of the classes defined in this library.
                    Assembly aLibrary = Assembly.LoadFrom(strOneFile);
                    Type[] tyAllTypesInLibrary = aLibrary.GetTypes();
                    // Loop through all of the classes, only look at those that implement
                    // the interface ICompoundingPlugin
                    foreach (Type tyOneClass in tyAllTypesInLibrary)
                    {
                        if (tyOneClass.GetInterfaces().Contains(typeof(IHTTPMethodHandler)))
                        {
                            // Have a subclass of the plugin class. Create an instance of 
                            // that class and put it into the dictionary of plugin
                            IHTTPMethodHandler iNewMethodPlugin = (IHTTPMethodHandler)Activator.CreateInstance(tyOneClass);
                            dictPlugins.Add(iNewMethodPlugin.MethodName, iNewMethodPlugin);
                            
                        }
                    }
                }
            }
        }

            private void Form1_Load(object sender, EventArgs e)
        {
            // Load plugins and create the config dictionary.
            dictPlugins = new Dictionary<string, IHTTPMethodHandler>();
            // Config dictionary.
            dictConfig = new Dictionary<string, string>();
            dictConfig.Add("ServerName", "Practice HTTP Server");
            dictConfig.Add("DocumentRoot", @"D:\Nhi's projects\Final Project\OSTest");
            vLoadPlugins();
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            // Check whether we are starting or stoping.
            if (btnStartStop.Text == "Start")
            {
                // Set up a socket to listen for connection requests from browsers.
                // Listen on IP address 127.0.0.1, port number 8080.
                IPAddress ipaLocalHost = IPAddress.Parse("127.0.0.1");
                IPEndPoint ipeListen = new IPEndPoint(ipaLocalHost, 8080);
                socListen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socListen.Bind(ipeListen);
                socListen.Listen(5);
                // Start a thread to accept the connection requests.
                Task tAcceptConnReq = new Task(vAcceptConnection);
                tAcceptConnReq.Start();
                // Change the text on the button to "Stop".
                btnStartStop.Text = "Stop";
            }
            else
            {
                // Button is "Stop". Close the listenning socket and dispose of it,
                // then change the button text back to start.
                socListen.Close();
                socListen.Dispose();
                socListen = null;
                btnStartStop.Text = "Start";
            }
        }
        // Function to accept connection requests.
        private void vAcceptConnection()
        {
            // Loop forever.
            for(; ; )
            {
                // Accept the next connection request. Return the socket
                // used for that connection.
                try
                {
                    Socket socConnection = socListen.Accept();
                    // Start a new thread to handle this connection.
                    Task tHandleConn = new Task(() => vHandleConnection(socConnection));
                    tHandleConn.Start();
                } catch (Exception)
                {
                    // Socket closed. Exit from loop.
                    break;
                }
            }
        }
        private delegate void AppendDelegate(string strMsg);
        // Funtion to handle the connection. This is a connection from a browser,
        // which we assume will send a request.
        private void vHandleConnection(Socket socConnection)
        {
            // Get the stream associated with the socket.
            NetworkStream nsSocStream = new NetworkStream(socConnection);
            // Get the request from the browser.
            HTTPRequest hrRequest = HTTPRequest.Receive(nsSocStream);
            string strLog = DateTime.Now.ToString("R") + ": " + socConnection.RemoteEndPoint.ToString() + "\r\n";
            strLog += hrRequest.Method + " " + hrRequest.URI + " " + hrRequest.Version;
            AppendDelegate adRTB = new AppendDelegate(rtbLog.AppendText);
            this.Invoke(adRTB, strLog + "\r\n");
            
            // Use the request method name to look up the proper plugin to handle the request.
            HTTPResponse hrResponse;
            if (dictPlugins.Keys.Contains(hrRequest.Method))
            {
                // Method name is in plugin dictionary. Get the corresponding plugin.
                IHTTPMethodHandler mhPlugin = dictPlugins[hrRequest.Method];
                // Use the plugin's GenResponse method to generate the response.
                hrResponse = mhPlugin.GenResponse(new ServerState(hrRequest, dictConfig, dictPlugins));
            }
            else
            {
                // Request's method name is not in the plugin dictionary. Generate a special 
                // error respone to send back to the server.
                string strStatus = "HTTP / 1.1 405 Method Not Allowed";

                List<string> lstrHeaders = new List<string>();
                string strShortDate = DateTime.Now.ToString("R");
                lstrHeaders.Add("Date: " + strShortDate);
                lstrHeaders.Add("Server: " + dictConfig["ServerName"]);
                string strAvaiMethod = "";
                foreach (var plugin in dictPlugins.Values)
                {
                    strAvaiMethod += plugin.MethodName + " ";
                }
                lstrHeaders.Add("Allow: " + strAvaiMethod.TrimEnd());
                lstrHeaders.Add("Connection: close");

                hrResponse = new HTTPResponse(strStatus, lstrHeaders, null);

            }
            // Use the response's to Send method to send the response back to the browser.
            hrResponse.Send(nsSocStream);
            // Shutdown the connection, close and dispose of the socket, and we are finished.
            socConnection.Shutdown(SocketShutdown.Both);
            socConnection.Close();
            socConnection.Dispose();
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If we still listening for connection requests, close the listening
            // socket and dispose of it.
            if (btnStartStop.Text == "Stop")
            {
                socListen.Close();
                socListen.Dispose();
            }
        }

    }
    public class ServerState : IServerState
    {
        private HTTPRequest hrRequest;
        private Dictionary<string, string> DictConfig;
        private Dictionary<string, IHTTPMethodHandler> DictPlugins;

        public HTTPRequest Request
        {
            get
            {
                return hrRequest;
            }
        }
        public Dictionary<string, string> ConfigDict
        {
            get
            {
                return DictConfig;
            }
        }
        public Dictionary<string, IHTTPMethodHandler> PluginDict
        {
            get
            {
                return DictPlugins;
            }
        }

        public ServerState(HTTPRequest hrR, Dictionary<string,string> ConfigD, Dictionary<string,IHTTPMethodHandler> PlugD)
        {
            hrRequest = hrR;
            DictConfig = ConfigD;
            DictPlugins = PlugD;
        }

    }
}
